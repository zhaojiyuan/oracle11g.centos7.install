# Oracle数据库安装脚本

在linux系统中安装oracle数据库安装需要执行大量的脚本，且手动安装及其容易出错，为了简化操作，写了一键静默安装脚本。

#### Oracle11G

- 系统环境版本：CentOS Linux release 7.9.2009 (其他版本没有进行测试)

- Oracle数据库版本：Oracle Database 11g Enterprise Edition Release 11.2.0.4.0 - 64bit Production

- 安装详细教程：[点我](./oracle11G/oracle11GInstall.md)

#### Oracle12G

- 系统环境版本：CentOS Linux release 7.9.2009 (其他版本没有进行测试)

- Oracle数据库版本：Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

- 安装详细教程：[点我](./oracle12C/oracle12cInstall.md)



#### 问题反馈

1.  有问题可以反馈，我也再努力学习中
2.  xxxx
3.  xxxx
4.  xxxx

